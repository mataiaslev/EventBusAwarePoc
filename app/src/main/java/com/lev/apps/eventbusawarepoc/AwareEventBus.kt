package com.lev.apps.eventbusawarepoc

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import com.squareup.otto.Bus


/**
 * Created by nanlabs on 05/02/18.
 */
class AwareEventBus(val mainActivity: MainActivity): LifecycleObserver {

    val bus = Bus()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun resume() {
        bus.register(mainActivity)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pause() {
        bus.unregister(mainActivity)
    }

    fun postOne() {
        bus.post(AnswerAvailableEvent(1))
    }

    fun postTwo() {
        bus.post(AnswerAvailableEvent(2))
    }

}