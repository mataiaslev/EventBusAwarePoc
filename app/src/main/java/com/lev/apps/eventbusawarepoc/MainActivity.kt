package com.lev.apps.eventbusawarepoc

import android.arch.lifecycle.Lifecycle
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.activity_main.*
import android.arch.lifecycle.LifecycleRegistry
import android.support.annotation.NonNull






class MainActivity : AppCompatActivity() {

    private lateinit var mLifecycleRegistry: LifecycleRegistry
    private val awareEventBus = AwareEventBus(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mLifecycleRegistry = LifecycleRegistry(this)
        mLifecycleRegistry.markState(Lifecycle.State.CREATED)

        lifecycle.addObserver(awareEventBus)
    }

    override fun getLifecycle(): Lifecycle {
        return mLifecycleRegistry
    }

    @Subscribe
    fun answerAvailable(event: AnswerAvailableEvent) {
        textView.text = event.int.toString()
    }

    fun setTextToNumberOne(view: View) {
        awareEventBus.postOne()
    }

    fun setTextToNumberTwo(view: View) {
        awareEventBus.postTwo()
    }

}

data class AnswerAvailableEvent(val int: Int)
